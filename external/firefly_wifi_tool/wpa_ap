#!/bin/sh
# Script to start/stop a hostapd-based access point
#
# Sample start call "control_ap start wlan0 eth0"
# Stop with "control_ap stop"
#
case "$1" in
start)
        if [ $# -ne 3 ]
        then
          echo "Usage: $0 start AP_iface NET_iface"
          exit 1
        fi
;;
stop)
        if [ $# -ne 1 ]
        then
          echo "Usage: $0 stop"
          exit 1
        fi
;;
*)
        echo "Usage:"
        echo "$0 start AP-iface net_iface"
        echo "or"
        echo "$0 stop"
        exit 1
        ;;
esac

# Symbols for AP and external interfaces

NET_AP=$2
NET_EXT=$3

# First 3 octets of IP address for the AP

AP_ADDR=192.168.3

# IP address for nameserver

NAME_SERVER=8.8.8.8

# AP Channel, SSID, Encryption method, driver,  and Encryption secret

AP_CHANNEL=11
AP_SSID=Firefly_AP
WPA_SECRET="12345678"
ENCRYPT_MODE=2
DRIVER=nl80211

case "$1" in
    start)
        echo "Starting AP mode for $NET_AP at address $AP_ADDR.1"
        # Disable packet forwarding
        echo 0 > /proc/sys/net/ipv4/ip_forward
        # first stop wpa_supplicant to stop staion mode
        killall -q wpa_supplicant
        # stop hostapd
        killall -q hostapd
        # Set up forwarding
        iptables -t nat -A POSTROUTING -o $NET_EXT -j MASQUERADE
        iptables -A FORWARD -i $NET_EXT -o $NET_AP -m state \
            --state RELATED,ESTABLISHED -j ACCEPT
        iptables -A FORWARD -i $NET_AP -o $NET_EXT -j ACCEPT
        # Get the AP interface in the right state
        ifconfig $NET_AP down
        sleep 1
        ifconfig $NET_AP up
        sleep 1
        ifconfig $NET_AP $AP_ADDR.1
        sleep 1

        # restart dhcp server
        killall -q dhcpd
        sleep 1
        dhcpd -cf /etc/dhcp/dhcpd.conf
        
        cat > /root/hostapd.conf << EOF
auth_algs=1
beacon_int=100
country_code=US
ctrl_interface_group=0
ctrl_interface=/var/run/hostapd
dtim_period=2
dump_file=/tmp/hostapd.dump
fragm_threshold=2346
#ht_capab=[HT40-][SHORT-GI-20][SHORT-GI-40][MAX-AMSDU-7935][DSSS_CCK-40]
#ieee80211d=1
ieee80211n=1
ignore_broadcast_ssid=0
logger_stdout=-1
logger_stdout_level=2
logger_syslog=-1
logger_syslog_level=2
macaddr_acl=0
max_num_sta=255
rts_threshold=2347
wmm_ac_be_acm=0
wmm_ac_be_aifs=3
wmm_ac_be_cwmax=10
wmm_ac_be_cwmin=4
wmm_ac_be_txop_limit=0
wmm_ac_bk_acm=0
wmm_ac_bk_aifs=7
wmm_ac_bk_cwmax=10
wmm_ac_bk_cwmin=4
wmm_ac_bk_txop_limit=0
wmm_ac_vi_acm=0
wmm_ac_vi_aifs=2
wmm_ac_vi_cwmax=4
wmm_ac_vi_cwmin=3
wmm_ac_vi_txop_limit=94
wmm_ac_vo_acm=0
wmm_ac_vo_aifs=2
wmm_ac_vo_cwmax=3
wmm_ac_vo_cwmin=2
wmm_ac_vo_txop_limit=47
wmm_enabled=1
EOF
	    echo "interface=$NET_AP" >> /root/hostapd.conf
	    echo "ssid=$AP_SSID" >> /root/hostapd.conf 
        echo "driver=$DRIVER" >> /root/hostapd.conf 
        echo "hw_mode=g" >> /root/hostapd.conf 
        echo "channel=$AP_CHANNEL" >> /root/hostapd.conf 
        echo "wpa=$ENCRYPT_MODE" >> /root/hostapd.conf 
        echo "wpa_key_mgmt=WPA-PSK" >> /root/hostapd.conf 
        echo "wpa_pairwise=TKIP CCMP" >> /root/hostapd.conf 
        echo "rsn_pairwise=CCMP" >> /root/hostapd.conf
        echo "wpa_passphrase=$WPA_SECRET" >> /root/hostapd.conf 
        # Enable packet forwarding
        echo 1 > /proc/sys/net/ipv4/ip_forward
        # Bring up hostapd
        hostapd -dd -B /root/hostapd.conf
        ;;

    stop)
        echo "Stopping AP mode"
        # Stop hostapd daemons
        killall hostapd
        rm -f /root/hostapd.conf
        ;;
esac

